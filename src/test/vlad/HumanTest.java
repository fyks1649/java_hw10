package vlad;

import org.hamcrest.core.Is;
import org.junit.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class HumanTest {
    private Man human = new Man("Vlad", "Lietun", 1995, 100, new String[][]{{String.valueOf(DayOfWeek.MONDAY)}, {"To do smth"}});
    private Man human1 = new Man("Denis", "Lietun", 1995, 100, new String[][]{{String.valueOf(DayOfWeek.MONDAY)}, {"To do smth"}});

    public String getExpectedToString(Human human) {
        String result = "Human{name=" + human.getName() +
                ", surname=" + human.getSurname() +
                ", year=" + human.getYear() +
                ", iq=" + human.getIq() +
                ", schedule=" + Arrays.deepToString(human.getSchedule()) + "}";
        return result;
    }

    @Test
    public void testToString(){
        Assert.assertEquals(human.toString(), getExpectedToString(human));
    }
}