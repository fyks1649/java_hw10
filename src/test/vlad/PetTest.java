package vlad;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class PetTest {
    private Dog pet = new Dog("Tuzyk", 5, 50, new String[]{"layat"});
    private Dog pet1 = new Dog("Tuzyk", 5, 50, new String[]{"layat"});

    public String getExpectedToString(Pet pet) {
        String result = pet.getSpecies() +
                "{nickname='" + pet.getNickname() +
                "', age=" + pet.getAge() +
                ", trickLevel=" + pet.getTrickLevel() +
                ", habits=" + Arrays.toString(pet.getHabits()) + "}";
        return result;
    }

    @Test
    public void testToString(){
        Assert.assertEquals(pet.toString(), getExpectedToString(pet));
    }
}