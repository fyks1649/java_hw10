package vlad;

public class DomesticCat extends Pet implements Foul {

    public DomesticCat() {
        this.species = Species.CAT;
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.CAT;
    }

    @Override
    public void respond() {
        System.out.println("Mmmmrrr");
    }

    @Override
    public void foul() {
        System.out.println("meeoow!");
    }
}
