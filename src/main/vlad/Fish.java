package vlad;

public class Fish extends Pet{

    public Fish() {
        species = Species.FISH;
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        species = Species.FISH;
    }

    @Override
    public void respond() {
        System.out.println("My name is " + this.getNickname());
    }
}
