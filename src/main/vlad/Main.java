package vlad;

public class Main {

    public static void main(String[] args) {


        Woman human1 = new Woman("Masha", "Masha");
        Man human2 = new Man("Vlad", "Vlad");


        Family family = new Family(human1,human2);
        String[][] humanScedule = {
                {DayOfWeek.SATURDAY.name(), "to do"},
                {DayOfWeek.SUNDAY.name(), "to do 2"}
        };

        human1.setSchedule(humanScedule);
        human2.setSchedule(humanScedule);

        family.addChild(new Man("Vlad", "Lietun"));

        Fish fish1 = new Fish();
        System.out.println(fish1);

        Fish fish = new Fish("Dorry", 1, 10, new String[]{"asas"});
        Dog dog = new Dog("Tuzik", 1, 10, new String[]{"ggaav"});
        DomesticCat domesticCat = new DomesticCat();

        fish.respond();
        dog.respond();
        dog.foul();
        domesticCat.respond();
        domesticCat.foul();

        RoboCat roboCat = new RoboCat();
        roboCat.respond();

        human2.greetPet(dog);
        human2.repairCar();
        human1.makeup();
    }
}


