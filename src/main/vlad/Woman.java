package vlad;

public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname) {
        super(name, surname);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    void makeup() {
        System.out.println("подкраситься");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Приветик " + pet.getNickname());
    }
}

