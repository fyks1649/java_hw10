package vlad;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname) {
        super(name, surname);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    void repairCar() {
        System.out.println("чинить авто");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Привет, " + pet.getSpecies());
    }


}
